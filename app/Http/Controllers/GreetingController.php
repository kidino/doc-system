<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class GreetingController extends Controller
{
    function hello($name) {
        echo "Hello there, $name.";
    }

    function goodbye($name) {
        return view('goodbye', compact('name'));
    }
}