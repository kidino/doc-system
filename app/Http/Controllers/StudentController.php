<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    function listing() {
        $students = \App\Student::all();
        return view('student.listing', compact('students'));
    }

    function edit_form($id = null){
        if ($id != null) $student = \App\Student::find($id);
        if ($id == null) $student = new \App\Student();
        $title = ($id == null) ? 'Add Student' : 'Edit Student';
        return view('student.edit_form', compact('student', 'title'));
    }

    function save(Request $request) {
        if ($request->id) {
            $student = \App\Student::find($request->id);
        } else {
            $student = new \App\Student();
        }
        $student->name = $request->name;
        $student->email = $request->email;
        $student->save();
        return redirect('/student')->with('status', 'Success');
    }

    function delete($id) {
        $student = \App\Student::find($id);
        $student->delete();
        return redirect('/student')->with('status', 'Success');
    }
}