<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello/{name}', 'GreetingController@hello');
Route::get('/goodbye/{name}', 'GreetingController@goodbye');
Route::get('/student', 'StudentController@listing')->middleware('auth');
Route::get('/student/edit/{id?}', 'StudentController@edit_form')->middleware('auth');
Route::post('/student/save', 'StudentController@save')->middleware('auth');
Route::get('/student/delete/{id}', 'StudentController@delete')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
