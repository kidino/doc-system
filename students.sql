CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
);

INSERT INTO `students` (`id`, `name`, `email`) VALUES
	(1, 'Iszuddin', 'iszuddin@doc-system.test'),
	(2, 'Jasdy', 'jasdy@doc-system.test');
