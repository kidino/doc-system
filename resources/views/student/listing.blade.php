<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Students</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <a class="btn btn-info float-right" href="/student/edit">Add New Student</a>
                <h1>List of Students</h1>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>EMAIL</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($students as $student)
                        <tr>
                            <td>{{$student->id}}</td>
                            <td>{{$student->name}}</td>
                            <td>{{$student->email}}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="/student/edit/{{$student->id}}">EDIT</a>
                                <a class="btn btn-danger btn-sm" href="/student/delete/{{$student->id}}">DELETE</a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="3">No students found</td></tr>
                    @endforelse
                    </tbody>
                </table>

            </div><!-- /col -->
        </div><!-- /row -->
    </div><!-- /container -->
</body>
</html>